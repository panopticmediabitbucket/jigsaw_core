#ifdef _RUN_UNIT_TESTS_
#include "TestKnobProvider.h"

namespace ProjectTests {
	START_JGSW_KNOB_PROVIDER_PROPERTIES(TestKnobProvider)
		PROVIDER_NAMESPACE("Jigsaw::Test")
		IMPORT_NAMESPACE("Jigsaw::Test::Alt")
		END_JGSW_KNOB_PROVIDER_PROPERTIES(TestKnobProvider)
		REGISTER_JGSW_KNOB_PROVIDER(TestKnobProvider);

	START_JGSW_KNOB_PROVIDER_PROPERTIES(TestKnobProviderAlt)
		PROVIDER_NAMESPACE("Jigsaw::Test::Alt")
		END_JGSW_KNOB_PROVIDER_PROPERTIES(TestKnobProviderAlt)
		REGISTER_JGSW_KNOB_PROVIDER(TestKnobProviderAlt);

	std::string TestReceiver::GetString()
	{
		return *string;
	}
}
#endif
