#ifdef _RUN_UNIT_TESTS_
#ifndef _TEST_KNOB_PROVIDER_H_
#define _TEST_KNOB_PROVIDER_H_

#include "Debug/ScopedLoggingTimer.h"
#include "Injection/JigsawInjection.h"
#include "Entities/JigsawEntityClusterService.h"
#include "Injection/JigsawSlots.h"
#include "MockReaderService.h"
#include <string>

namespace ProjectTests {

	JGSW_SLOT_RECEIVER(TestIsAInitializedClass) {
	public:
		TestIsAInitializedClass(const Jigsaw::Ref<MockReaderService> & reader) : reader(reader) {}

		Jigsaw::Ref<MockReaderService> reader;
	};

	JGSW_KNOB_PROVIDER(TestKnobProvider) {

		KNOB(Jigsaw::Entities::JigsawEntityClusterService, cluster_service) {
			return new Jigsaw::Entities::JigsawEntityClusterService;
		};
	};

	JGSW_KNOB_PROVIDER(TestKnobProviderAlt) {

		KNOB(std::string, string) {
			return new std::string("test string value");
		};

		KNOB(MockReaderService, reader_service) {
			return new MockReaderService;
		};

		KNOB(MockReaderAccessor, reader_accessor,
			SLOT_ARG(MockReaderService, reader_service)) {
			return new MockReaderAccessor(reader_service);
		}
	};

	JGSW_SLOT_RECEIVER(TestReceiver) {
public:
	std::string GetString();

	INJECT_CONTEXT(jgsw_context);

	SLOT(MockReaderService, clock_reader);

private:
	SLOT(std::string, string);

	};
}
#endif
#endif
