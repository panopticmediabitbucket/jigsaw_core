#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Injection/JigsawInjection.h"
#include "TestKnobProvider.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {

	TEST_CLASS(JigsawKnobProviderTest) {

		TEST_METHOD(TestPropertiesRegistered) {
			std::vector<Jigsaw::Injection::knob_provider_definition> providers = Jigsaw::Injection::JigsawGlobalContext::GetNamespaceProviders("Jigsaw::Test");
			auto provider_def = providers.at(0);
			Jigsaw::Injection::JigsawKnobProvider* provider = provider_def.build_provider();

			TestKnobProvider* knob_provider = dynamic_cast<TestKnobProvider*>(provider);
			Assert::IsTrue((bool)knob_provider);

			Jigsaw::Injection::knob_definition knob_def = *provider->knobs.begin();
			Assert::IsTrue("cluster_service" == knob_def.name);
			knob_def.create_reference();
			Jigsaw::Ref<Jigsaw::Entities::JigsawEntityClusterService> cluster_service = *static_cast<Jigsaw::Ref<Jigsaw::Entities::JigsawEntityClusterService>*>(knob_def.reference_getter());
			Assert::IsNotNull(cluster_service.get());
		}

		TEST_METHOD(TestKnobProviderConstructor) {
			std::vector<Jigsaw::Injection::knob_provider_definition> providers = Jigsaw::Injection::JigsawGlobalContext::GetNamespaceProviders("Jigsaw::Test");
			auto provider_def = providers.at(0);
			Jigsaw::Injection::JigsawKnobProvider* provider = provider_def.build_provider();

			TestKnobProvider* knob_provider = dynamic_cast<TestKnobProvider*>(provider);

			Jigsaw::Injection::knob_definition knob_def = *std::find_if(provider->knobs.begin(), provider->knobs.end(), [](Jigsaw::Injection::knob_definition& def) { return def.name == "scene_hierarchy"; });
			Assert::AreEqual(3, (int)knob_def.constructor_dependencies.size());

		}
	};
}

#endif
