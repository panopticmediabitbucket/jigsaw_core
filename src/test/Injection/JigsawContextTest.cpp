#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Injection/JigsawContextConfig.h"
#include "Injection/JigsawContext.h"
#include "Entities/JigsawEntityClusterService.h"
#include "Injection/TestKnobProvider.h"
#include "Injection/JigsawInjection.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {
	JGSW_SLOT_RECEIVER(MockSystemsOrchestrator) {

	public:
		MockSystemsOrchestrator() {}

		SLOT(Jigsaw::Entities::JigsawEntityClusterService, cluster_service);

	};

	START_JGSW_COMPONENT_PROPERTIES(MockSystemsOrchestrator, orchestrator)
	PROVIDER_NAMESPACE("Jigsaw::Test::Alt")
	END_JGSW_COMPONENT_PROPERTIES(MockSystemsOrchestrator, orchestrator)
	JGSW_COMPONENT(MockSystemsOrchestrator, orchestrator);

	TEST_CLASS(JigsawContextTest)
	{

		static Jigsaw::Ref<Jigsaw::Injection::JigsawContext> context;

		TEST_CLASS_INITIALIZE(Setup) {
			Jigsaw::Injection::JigsawContextConfig config;
			config.package_names.push_back("Jigsaw::Test");

			context = Jigsaw::Injection::JigsawContext::Create(config);
		}

		TEST_METHOD(TestContextInitialization) {
			Jigsaw::Ref<Jigsaw::Entities::JigsawEntityClusterService> cluster_service = context->GetKnob<Jigsaw::Entities::JigsawEntityClusterService>();
			Assert::IsTrue((bool)cluster_service);
		}

		TEST_METHOD(TestTransitiveNamespace) {
			Jigsaw::Ref<std::string> string = context->GetKnob<std::string>();
			Assert::IsTrue(*string == "test string value");
		}

		TEST_METHOD(TestInjectKnobsIntoJigsawSlots) {
			Jigsaw::Ref<TestReceiver> receiver = Jigsaw::MakeRef<TestReceiver>();
			context->FillSlots(*receiver);
			Assert::IsTrue(receiver->GetString() == "test string value");
		}

		TEST_METHOD(TestKnobComponentPickedUp) {
			Jigsaw::Ref<MockSystemsOrchestrator> system_orchestrator = context->GetKnob<MockSystemsOrchestrator>();
			Jigsaw::Ref<Jigsaw::Entities::JigsawEntityClusterService> cluster_service = context->GetKnob<Jigsaw::Entities::JigsawEntityClusterService>();
			Assert::IsTrue(cluster_service.get() == system_orchestrator->cluster_service.get());
		}

		TEST_METHOD(TestSceneHierarachyBuiltWithParameters) {
			Jigsaw::Ref<MockReaderAccessor> reader_accessor = context->GetKnob<MockReaderAccessor>();
			Jigsaw::Ref<MockReaderService> reader_service = context->GetKnob<MockReaderService>();

			Assert::IsTrue(reader_service.get());
			Assert::IsTrue(reader_accessor->reader_service.get() == reader_service.get());
		}

		TEST_METHOD(TestContextInjected) {
			Jigsaw::Ref<TestReceiver> receiver = Jigsaw::MakeRef<TestReceiver>();
			context->FillSlots(*receiver);
			Jigsaw::Injection::JigsawContext* internal_context = receiver->jgsw_context.get();
			Assert::IsTrue(internal_context == context.get());
		}

		TEST_METHOD(TestIsAInheritance) {
			Jigsaw::Ref<TestReceiver> receiver = Jigsaw::MakeRef<TestReceiver>();
			context->FillSlots(*receiver);
			Assert::IsNotNull(receiver->clock_reader.get());
		}

		TEST_METHOD(TestIsAGraphResolution) {
			Jigsaw::Ref<TestIsAInitializedClass> init_class = context->GetKnob<TestIsAInitializedClass>();
			Assert::IsNotNull(init_class->reader.get());
		}
	};

	Jigsaw::Ref<Jigsaw::Injection::JigsawContext> JigsawContextTest::context;
}
#endif
