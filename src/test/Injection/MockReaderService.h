#ifdef _RUN_UNIT_TESTS_
#ifndef _MOCK_READER_SERVICE_H_

#include "Injection/JigsawInjection.h"
#include "Ref.h"

namespace ProjectTests {
	class MockReaderService {
	public:

	};

	JGSW_SLOT_RECEIVER(MockReaderAccessor) {
	public:
		MockReaderAccessor(const Jigsaw::Ref<MockReaderService> & service) {
			reader_service = service;
		}

		Jigsaw::Ref<MockReaderService> reader_service;
		SLOT(std::string, string);

	};

}

#endif
#endif