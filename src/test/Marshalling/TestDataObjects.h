#ifdef _RUN_UNIT_TESTS_
#ifndef _TEST_DATA_OBJECTS_

#include <vector>
#include "Entities/JigsawEntity.h"
#include "System/UID.h"
#include "Util/StringVector.h"
#include "Math/LinAlg.h"

namespace ProjectTests {
	class SerializableEntityData {
	public:
		Jigsaw::System::UID entity_id;
		Jigsaw::Entities::JigsawEntity::SCOPE scope;
		std::vector<Jigsaw::Util::etype_index> aligned_types;
		Jigsaw::Util::StringVector string_vector;

	};

	class SerializableMachineData {
	public:
		std::vector<SerializableEntityData> entity_data;

	};

	class Transform {

	public:
		Vector3 position;
		Vector3 scale;
		Quaternion rotation;

		Transform() {
			scale = Vector3(1, 1, 1);
			position = Vector3(0, 0, 0);
		}

		Transform& operator=(const Transform& other);

		bool operator==(const Transform& other);

		Mat4x4 getRotationMatrix() const;
		Mat4x4 getTRSMatrix() const;

	private:
		Transform* parent;

	};
}

#endif
#endif