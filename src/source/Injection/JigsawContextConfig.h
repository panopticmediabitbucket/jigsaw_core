#ifndef _JIGSAW_CONTEXT_CONFIG_H_
#define _JIGSAW_CONTEXT_CONFIG_H_

#include <vector>
#include <string>
#include "_jgsw_api.h"

namespace Jigsaw {
	namespace Injection {

		struct JGSW_API JigsawContextConfig {
			std::vector<std::string> package_names;

		};
	}
}

#endif