#ifndef _JIGSAW_KNOB_PROVIDER_H_
#define _JIGSAW_KNOB_PROVIDER_H_

#include "Injection/injection_definitions.h"
#include "Util/etype_info.h"
#include <functional>
#include "_jgsw_api.h"

namespace Jigsaw {
	namespace Injection {

		/// <summary>
		/// The JigsawKnobProvider is the base class for any provider/configuration class that instantiates
		/// JigsawKnobs. The 'knobs' field is populated with the help of macros located in JigsawInjection.h.
		/// </summary>
		class JGSW_API JigsawKnobProvider {
		public:
			virtual ~JigsawKnobProvider() {}

			std::list<knob_definition> knobs;

		};

	}
}



#endif