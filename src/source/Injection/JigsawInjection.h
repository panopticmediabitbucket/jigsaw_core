#ifndef _JIGSAW_INJECTION_H_
#define _JIGSAW_INJECTION_H_

#include "Debug/ScopedLoggingTimer.h"
#include "Injection/JigsawContext.h"
#include "Injection/JigsawKnobProvider.h"
#include "Injection/JigsawSlots.h"
#include "Util/DTO.h"

#ifndef _EXPAND
#define _EXPAND(...) __VA_ARGS__
#endif

#define START_JGSW_KNOB_PROVIDER_PROPERTIES(classname)\
namespace ___ {\
	Jigsaw::Injection::knob_provider_properties _EXPAND(classname)_properties() {\
		Jigsaw::Injection::knob_provider_properties properties;

#define START_JGSW_COMPONENT_PROPERTIES(classname, component)\
namespace ___ {\
	Jigsaw::Injection::knob_provider_properties _EXPAND(component)_properties() {\
		Jigsaw::Injection::knob_provider_properties properties;

#define PROVIDER_NAMESPACE(str) properties._namespace = str;

#define IMPORT_NAMESPACE(str) properties.import_namespaces.push_back(str);

#define END_JGSW_KNOB_PROVIDER_PROPERTIES(classname)\
		return properties;\
	}; \
}

#define END_JGSW_COMPONENT_PROPERTIES(classname, component)\
		return properties;\
	}; \
}

#define JGSW_KNOB_PROVIDER(classname) class classname : public Jigsaw::Injection::JigsawKnobProvider, virtual protected Jigsaw::Util::DTO<classname>
#define JGSW_SLOT_RECEIVER(classname) class classname : public Jigsaw::Injection::JigsawSlots, virtual protected Jigsaw::Util::DTO<classname>

#define REGISTER_JGSW_KNOB_PROVIDER(classname) \
struct _EXPAND(classname)_register_def { \
	_EXPAND(classname)_register_def() {\
		Jigsaw::Injection::knob_provider_definition provider_definition; \
		provider_definition.properties = ___::_EXPAND(classname)_properties(); \
		provider_definition.build_provider = [] () -> Jigsaw::Injection::JigsawKnobProvider* { return new classname; }; \
		provider_definition.name = #classname; \
		Jigsaw::Injection::JigsawGlobalContext::SubmitProvider(provider_definition); \
	}\
}; \
_EXPAND(classname)_register_def _EXPAND(classname)_register_def_call;

#define JGSW_COMPONENT(classname, component) \
struct _EXPAND(component)_register_def { \
	_EXPAND(component)_register_def() {\
		Jigsaw::Injection::knob_provider_definition provider_definition; \
		provider_definition.properties = ___::_EXPAND(component)_properties(); \
		provider_definition.build_provider = [] () -> Jigsaw::Injection::JigsawKnobProvider* { \
			Jigsaw::Injection::JigsawKnobProvider* provider = new Jigsaw::Injection::JigsawKnobProvider; \
			Jigsaw::Ref<classname>* _component = new Jigsaw::Ref<classname>(); \
			Jigsaw::Injection::knob_definition definition; \
			definition.reference_getter = [_component] () -> void* { return (void*)_component; }; \
			definition.reference_present = [=] () -> bool { return (bool)*_component; }; \
			definition.create_reference = [=] () { *_component = Jigsaw::MakeRef<classname>(); }; \
			definition.type_info = &Jigsaw::Util::etype_info::Id<classname>(); \
			definition.get_slots = [_component] (std::vector<Jigsaw::Injection::slot_definition>* slot_defs) { \
				if(std::is_base_of_v<Jigsaw::Injection::JigsawSlots, classname>) { \
					*slot_defs = Jigsaw::Injection::GetSlots(*(Jigsaw::Injection::JigsawSlots*)(void*)_component->get()); \
					return true; \
				} \
				return false; \
			}; \
			provider->knobs.push_back(definition); \
			return provider; \
		}; \
		Jigsaw::Injection::JigsawGlobalContext::SubmitProvider(provider_definition); \
	}\
}; \
_EXPAND(component)_register_def _EXPAND(component)_register_def_call;

#define _SLOT(classname, type_name, qual) { Jigsaw::Ref<classname> * ref = new Jigsaw::Ref<classname>; Jigsaw::Injection::slot_definition slot_def; slot_def.name = #type_name; \
 slot_def.type_info = &Jigsaw::Util::etype_info::Id<classname>(); slot_def.reference_setter = [ref] (void* reference) -> void { *ref = *static_cast<Jigsaw::Ref<classname>*>(reference); }; \
slot_def.reference_present = [ref] () -> bool { return (bool)ref->get(); }; \
slot_def.reference_getter = [ref] () -> void* { return ref; }; definition.constructor_dependencies.push_back(slot_def); }

#define _IS_A(classname, type_name, qual) if constexpr (std::is_base_of<type_name, classname>::value) { definition.super_classes.push_back(Jigsaw::Util::etype_info::Id<type_name>()); } \
else { J_D_ASSERT_LOG_ERROR(false, Jigsaw::Injection::knob_definition, "'IS_A' macro invalid for knob definition '{0}', the passed in type must be a super class of type {1}", definition.name, Jigsaw::Util::etype_info::Id<classname>().GetQualifiedName()); }

#define _COMMA ,

#define _HARVEST_SLOT(classname, type_name, qual, ...) *static_cast<Jigsaw::Ref<classname>*>((*std::find_if(definition.constructor_dependencies.begin(), definition.constructor_dependencies.end(), [&](const Jigsaw::Injection::slot_definition& slot_def) { return slot_def.type_info == &Jigsaw::Util::etype_info::Id<classname>(); } )).reference_getter()) __VA_ARGS__
#define _HARVEST_IS_A(type_name, classname, qual, ...) 

#define _ARG_SLOT(classname, type_name, qual, ...) Jigsaw::Ref<classname> type_name __VA_ARGS__
#define _ARG_IS_A(classname, type_name, qual, ...)

#define _inj_mac_fe_1(mac, a, b, c, ...) mac(a, b, c)
#define _inj_mac_fe_2(mac, a, b, c, ...) mac(a, b, c) _EXPAND(_inj_mac_fe_1(__VA_ARGS__))
#define _inj_mac_fe_3(mac, a, b, c, ...) mac(a, b, c) _EXPAND(_inj_mac_fe_2(__VA_ARGS__))
#define _inj_mac_fe_4(mac, a, b, c, ...) mac(a, b, c) _EXPAND(_inj_mac_fe_3(__VA_ARGS__))
#define _inj_mac_fe_5(mac, a, b, c, ...) mac(a, b, c) _EXPAND(_inj_mac_fe_4(__VA_ARGS__))
#define _inj_mac_fe_6(mac, a, b, c, ...) mac(a, b, c) _EXPAND(_inj_mac_fe_5(__VA_ARGS__))
#define _inj_mac_fe_7(mac, a, b, c, ...) mac(a, b, c) _EXPAND(_inj_mac_fe_6(__VA_ARGS__))
#define _inj_mac_fe_8(mac, a, b, c, ...) mac(a, b, c) _EXPAND(_inj_mac_fe_7(__VA_ARGS__))

#define _inj_arg_mac_fe_1(mod, mac, a, b, c, ...) _EXPAND(_EXPAND(mod)mac)(a, b, c)
#define _inj_arg_mac_fe_2(mod, mac, a, b, c, ...) _EXPAND(_EXPAND(mod)mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_1(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_3(mod, mac, a, b, c, ...) _EXPAND(_EXPAND(mod)mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_2(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_4(mod, mac, a, b, c, ...) _EXPAND(_EXPAND(mod)mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_3(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_5(mod, mac, a, b, c, ...) _EXPAND(_EXPAND(mod)mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_4(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_6(mod, mac, a, b, c, ...) _EXPAND(_EXPAND(mod)mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_5(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_7(mod, mac, a, b, c, ...) _EXPAND(_EXPAND(mod)mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_6(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_8(mod, mac, a, b, c, ...) _EXPAND(_EXPAND(mod)mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_7(mod, __VA_ARGS__))

#define _INJ_ARG_LOOP(_1, __1, ___1, ____1, _2, __2, ___2, ____2, _3, __3, ___3, ____3, _4, __4, ___4, ____4, _5, __5, ___5, ____5, _6, __6, ___6, ____6, _7, __7, ___7, ____7, _8, __8, ___8, ____8, N, ...)N

#define SLOT_ARG(classname, type_name) _SLOT, classname, type_name, ""
#define QUALIFIER_ARG(classname, type_name) _QUAL, classname, type_name, ""
#define IS_A(type_name) _IS_A, _KNOB_TYPE, type_name, ""
#define IS_AN(type_name) _IS_A, _KNOB_TYPE, type_name, ""

#define _NONE(...) 

#define KNOB(type_name, knob_name, ...) \
struct _EXPAND(knob_name)_knob_def { \
	_EXPAND(knob_name)_knob_def(_THIS_TYPE& provider) { \
		typedef type_name _KNOB_TYPE; \
		Jigsaw::Injection::knob_definition definition; \
		definition.name = #knob_name; \
		definition.type_info = &Jigsaw::Util::etype_info::Id<type_name>(); \
		_EXPAND(_INJ_ARG_LOOP(__VA_ARGS__ _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_1, _inj_mac_fe_1, _inj_mac_fe_1, _inj_mac_fe_1, _NONE)(__VA_ARGS__)) \
		Jigsaw::Ref<type_name>** knob_ref = new Jigsaw::Ref<type_name>* { nullptr }; \
		definition.create_reference = [=] () -> void { *knob_ref = new Jigsaw::Ref<type_name>(\
_THIS_TYPE::_EXPAND(knob_name)_get(\
		_EXPAND(_INJ_ARG_LOOP(__VA_ARGS__ _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _NONE)(_HARVEST, __VA_ARGS__)) \
)); }; \
		definition.reference_getter = [=] () -> void* { return (void*)*knob_ref; }; \
		definition.reference_present = [=] () -> bool { return *knob_ref != nullptr; }; \
		definition.get_slots = [=] (std::vector<Jigsaw::Injection::slot_definition>* slot_defs) { \
			if(std::is_base_of_v<Jigsaw::Injection::JigsawSlots, type_name>) { \
				*slot_defs = Jigsaw::Injection::GetSlots(*(Jigsaw::Injection::JigsawSlots*)(void*)(*knob_ref)->get()); \
				return true; \
			} \
			return false; \
		 }; \
		provider.knobs.insert(provider.knobs.end(), std::move(definition)); \
	}; \
}; \
_EXPAND(knob_name)_knob_def _EXPAND(knob_name)_knob = _EXPAND(knob_name)_knob_def(*this); \
static _EXPAND(type_name)* _EXPAND(knob_name)_get( \
		_EXPAND(_INJ_ARG_LOOP(__VA_ARGS__ _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _NONE)(_ARG, __VA_ARGS__)) \
) 

#define SLOT(type_name, slot_name) \
struct _EXPAND(slot_name)_slot_def { \
	_EXPAND(slot_name)_slot_def(_THIS_TYPE& receiver) { \
		Jigsaw::Injection::slot_definition definition; \
		definition.name = #slot_name; \
		definition.type_info = &Jigsaw::Util::etype_info::Id<type_name>(); \
		definition.reference_setter = [&receiver] (void* reference) -> void { receiver._EXPAND(slot_name) = *static_cast<Jigsaw::Ref<type_name>*>(reference); }; \
		definition.reference_present = [&receiver] () { return (bool)receiver._EXPAND(slot_name); }; \
		receiver.slots.push_back(definition); \
	}; \
}; \
_EXPAND(slot_name)_slot_def _EXPAND(slot_name)_slot = _EXPAND(slot_name)_slot_def(*this); \
Jigsaw::Ref<_EXPAND(type_name)> _EXPAND(slot_name)

#define INJECT_CONTEXT(variable_name) \
SLOT(Jigsaw::Injection::JigsawContext, variable_name)

#endif