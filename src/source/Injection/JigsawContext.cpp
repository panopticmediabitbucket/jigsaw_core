#include "JigsawContext.h"
#include <map>
#include <algorithm>
#include "Util/VectorUtils.h"
#include "Debug/j_debug.h"
#include "Debug/ScopedLoggingTimer.h"

namespace Jigsaw {
	namespace Injection {
		std::map<std::string, std::vector<knob_provider_definition>>& GetProviderMap() {
			static std::map<std::string, std::vector<knob_provider_definition>> map;
			return map;
		}

		void JigsawGlobalContext::SubmitProvider(const knob_provider_definition& provider_definition) {
			std::map<std::string, std::vector<knob_provider_definition>>& map = GetProviderMap();
			auto find = map.find(provider_definition.properties._namespace);
			if (find == map.end()) {
				map.insert(std::make_pair(provider_definition.properties._namespace, std::vector<knob_provider_definition>()));
			}
			map.at(provider_definition.properties._namespace).push_back(provider_definition);
		}

		std::vector<knob_provider_definition> JigsawGlobalContext::GetNamespaceProviders(const std::string& _namespace) {
			std::map<std::string, std::vector<knob_provider_definition>>& map = GetProviderMap();
			auto find = map.find(_namespace);
			if (find != map.end()) {
				return (*find).second;
			}
			return std::vector<knob_provider_definition>();
		}

		// JigsawContext implementations

		/// <summary>
		/// An acyclic graph with multiple edge types. The graph is lazily initialized--all the nodes are grouped into a vector,
		/// and then the edges are traversed at the same time that they're discovered and that the acyclic rules are enforced. 
		/// </summary>
		class KnobGraph {
		public:
			KnobGraph(Jigsaw::Ref<JigsawContext> context_bean, const std::vector<std::string>& namespaces) : namespaces(namespaces) {
				knob_definition context_def;
				context_def.reference_present = []() { return true; };
				context_def.reference_getter = [=]() -> void* { return (void*)&context_bean; };
				context_def.type_info = &Jigsaw::Util::etype_info::Id<JigsawContext>();
				context_def.name = "root_context";

				Jigsaw::Ref<_node> node = Jigsaw::MakeRef<_node>();
				node->knob_def = context_def;
				nodes.push_back(node);
			}

			std::vector<knob_definition> Resolve() {

				for (int i = 0; i < namespaces.size(); i++) {
					std::vector<knob_provider_definition> knob_provider_defs = Jigsaw::Injection::JigsawGlobalContext::GetNamespaceProviders(namespaces.at(i));

					START_TIMER_SCOPE(KnobGraph, "Instantiating the Knob Providers for namespace " + namespaces.at(i));
					// instantiating the knob providers
					std::vector<Jigsaw::Ref<Jigsaw::Injection::JigsawKnobProvider>> knob_providers
						= std::map_to<knob_provider_definition, Jigsaw::Ref<Jigsaw::Injection::JigsawKnobProvider>>(knob_provider_defs,
							[](knob_provider_definition& _def) { 

								START_TIMER_SCOPE(KnobGraph, "Instantiating a KnobProvider: " + _def.name)
								return Jigsaw::Ref<Jigsaw::Injection::JigsawKnobProvider>(_def.build_provider()); 
								END_TIMER_SCOPE
							});

					for (Jigsaw::Ref<Jigsaw::Injection::JigsawKnobProvider>& provider : knob_providers) {

						// populating the map from the providers
						std::for_each(provider->knobs.begin(), provider->knobs.end(), [this](Jigsaw::Injection::knob_definition& knob_def) {
							Jigsaw::Ref<_node> node = Jigsaw::MakeRef<_node>();
							node->knob_def = knob_def;
							nodes.push_back(node);
							});
					}
					END_TIMER_SCOPE
				}

				START_TIMER_SCOPE(KnobGraph, "Resolving dependencies for each node in the graph")
				for (Jigsaw::Ref<_node>& node : nodes) {
					ResolveDependencies(node);
				}
				END_TIMER_SCOPE

				std::vector<knob_definition> knobs;
				std::for_each(nodes.begin(), nodes.end(), [&knobs](Jigsaw::Ref<_node>& node) -> void { knobs.push_back(node->knob_def); });
				return knobs;
			};

		private:
			struct _node {
				knob_definition knob_def;
				std::vector<Jigsaw::Ref<_node>> constructor_dependencies;
				std::vector<Jigsaw::Ref<_node>> slot_dependencies;
			};

			/// <summary>
			/// Utility function for locating a knob after a starting point 'start_iterator' that could fill the slot_definition 'definition'
			/// </summary>
			/// <param name="start_iterator"></param>
			/// <param name="knobs"></param>
			/// <param name="definition"></param>
			/// <returns></returns>
			std::vector<Jigsaw::Ref<_node>>::const_iterator FindMatchingDefinition(std::vector<Jigsaw::Ref<_node>>::const_iterator start_iterator,
				const std::vector<Jigsaw::Ref<_node>>& knobs, const Jigsaw::Injection::slot_definition& definition) {

				std::vector<Jigsaw::Ref<_node>>::const_iterator ret_iter = start_iterator;

				bool match = false;
				do {
					std::vector<Jigsaw::Ref<_node>>::const_iterator this_iter = ret_iter;
					match = (*this_iter)->knob_def.type_info == definition.type_info;
					if (!match) {
						auto new_iter = std::find_if((*this_iter)->knob_def.super_classes.begin(), (*this_iter)->knob_def.super_classes.end(), [&definition](const Jigsaw::Util::etype_index& index) { return index.Get() == *definition.type_info; });
						match |= new_iter != (*this_iter)->knob_def.super_classes.end();
					}
				} while (!match && ++ret_iter != knobs.end());

				return ret_iter;
			}

			/// <summary>
			/// This method locates a corresponding node that can be used to fill the each of defined dependencies and pushes a reference
			/// to that node into the node_dependency_container
			/// </summary>
			/// <param name="dependencies"></param>
			/// <param name="node_dependency_container"></param>
			/// <param name="knob_name"></param>
			void PlaceNodeDependenciesInContainer(std::vector<Jigsaw::Injection::slot_definition>& dependencies, std::vector<Jigsaw::Ref<_node>>& node_dependency_container, std::string& knob_name) {
				for (Jigsaw::Injection::slot_definition& slot : dependencies) {
					auto iter = FindMatchingDefinition(nodes.begin(), nodes, slot);

					J_D_ASSERT_LOG_ERROR((iter != nodes.end()), KnobGraph, "No knob matching the type {0} found to fill dependency {1}", slot.type_info->GetQualifiedName(), knob_name);
					node_dependency_container.push_back(*iter);
				}
			}

			/// <summary>
			/// This method iterates through the slot_dependencies and their corresponding _node dependencies which were linked
			/// with the LinkDependencies method. There should be an exact matching number of dependencies. 
			///
			/// This method is used for both constructor dependencies and slot dependencies. It is also recursive; the other ResolveDependencies
			/// method is called to ensure that transitive dependencies are resolved before constructing an object.
			/// </summary>
			/// <param name="slot_dependencies"></param>
			/// <param name="graph_dependencies"></param>
			void ResolveDependencies(std::vector<Jigsaw::Injection::slot_definition>& slot_dependencies, std::vector<Jigsaw::Ref<_node>>& graph_dependencies) {

				auto iter = graph_dependencies.begin();
				auto slot_iter = slot_dependencies.begin();
				while (iter != graph_dependencies.end()) {
					Jigsaw::Ref<_node>& dependency = *iter;
					ResolveDependencies(dependency);
					slot_definition& slot = *slot_iter;
					if (!slot.reference_present()) {
						knob_definition& knob_def = dependency->knob_def;
						slot.reference_setter(knob_def.reference_getter());
					}
					iter++;
					slot_iter++;
				}
			}

			void ResolveDependencies(Jigsaw::Ref<_node>& node) {
				if (!node->knob_def.reference_present()) {
					// first resolve dependencies needed to construct the object
					PlaceNodeDependenciesInContainer(node->knob_def.constructor_dependencies, node->constructor_dependencies, node->knob_def.name);
					ResolveDependencies(node->knob_def.constructor_dependencies, node->constructor_dependencies);

					// construct the object
					node->knob_def.create_reference();

					// resolve slot dependencies attached to the object
					std::vector<Jigsaw::Injection::slot_definition> slots;
					if (node->knob_def.get_slots(&slots)) {
						PlaceNodeDependenciesInContainer(slots, node->slot_dependencies, node->knob_def.name);
						ResolveDependencies(slots, node->slot_dependencies);
					}
				}
			};

			std::vector<Jigsaw::Ref<_node>> nodes;
			const std::vector<std::string>& namespaces;
		};

		Jigsaw::Ref<JigsawContext> JigsawContext::Create(const JigsawContextConfig& config) {
			Jigsaw::Ref<JigsawContext> context = Jigsaw::Ref<JigsawContext>(new JigsawContext);
			context->Init(context, config);
			return context;
		}

		void JigsawContext::Init(Jigsaw::Weak<JigsawContext> self_reference, const JigsawContextConfig& config) {
			self = self_reference;

			std::for_each(config.package_names.begin(), config.package_names.end(), [this](const std::string& def) { this->namespaces.push_back(def); });

			for (int i = 0; i < namespaces.size(); i++) {
				std::vector<knob_provider_definition> knob_provider_defs = Jigsaw::Injection::JigsawGlobalContext::GetNamespaceProviders(namespaces.at(i));
				for (knob_provider_definition& provider_def : knob_provider_defs) {

					for (std::string& _namespace : provider_def.properties.import_namespaces) {
						if (std::find(namespaces.begin(), namespaces.end(), _namespace) == namespaces.end()) {
							this->namespaces.push_back(_namespace);
						}
					}
				}
			}

			START_TIMER_SCOPE(JigsawContext, "Resolving the JigsawContext's KnobGraph")
			KnobGraph graph(self_reference.lock(), namespaces);
			knobs = graph.Resolve();
			END_TIMER_SCOPE
		}

		JigsawContext::JigsawContext() { }

		/// <summary>
		/// Utility function for locating a knob after a starting point 'start_iterator' that could fill the slot_definition 'definition'
		/// </summary>
		/// <param name="start_iterator"></param>
		/// <param name="knobs"></param>
		/// <param name="definition"></param>
		/// <returns></returns>
		std::vector<Jigsaw::Injection::knob_definition>::const_iterator FindMatchingDefinition(std::vector<Jigsaw::Injection::knob_definition>::const_iterator start_iterator,
			const std::vector<Jigsaw::Injection::knob_definition>& knobs, const Jigsaw::Injection::slot_definition& definition) {

			std::vector<Jigsaw::Injection::knob_definition>::const_iterator ret_iter = start_iterator;

			bool match = false;
			do {
				std::vector<Jigsaw::Injection::knob_definition>::const_iterator this_iter = ret_iter;
				match = (*this_iter).type_info == definition.type_info;
				if (!match) {
					auto new_iter = std::find_if((*this_iter).super_classes.begin(), (*this_iter).super_classes.end(), [&definition](const Jigsaw::Util::etype_index& index) { return index.Get() == *definition.type_info; });
					match |= new_iter != (*this_iter).super_classes.end();
				}
			} while (!match && ++ret_iter != knobs.end());

			return ret_iter;
		}

		bool JigsawContext::FillSlot(const Jigsaw::Injection::slot_definition& definition) const {
			auto begin = knobs.begin();
			begin = FindMatchingDefinition(begin, knobs, definition);

			if (begin != knobs.end()) {
				J_D_ASSERT_LOG_ERROR((FindMatchingDefinition(begin + 1, knobs, definition) == knobs.end()), JigsawContext, "Multiple matching knobs for slot found");

				// put knob to slot
				definition.reference_setter((*begin).reference_getter());

				return true;
			}

			return false;
		}

		void JigsawContext::FillSlots(const std::vector<Jigsaw::Injection::slot_definition>& slot_definitions, const std::string& receiver_name) const {
			for (const Jigsaw::Injection::slot_definition& slot_def : slot_definitions) {
				if (!FillSlot(slot_def)) {
					J_D_ASSERT_LOG_ERROR(false, JigsawContext, "No matching knob of type {0} was found while filling the slots of knob {1}", slot_def.type_info->GetQualifiedName(), receiver_name);
				}
			}
		}

		void JigsawContext::FillSlots(Jigsaw::Injection::JigsawSlots& slots_class) const {
			const std::vector<slot_definition>& slots = GetSlots(slots_class);
			FillSlots(slots, "Anonymous JigsawSlots object");
		}
	}
}

