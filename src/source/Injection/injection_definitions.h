#ifndef _INJECTION_DEFINITIONS_H_
#define _INJECTION_DEFINITIONS_H_

#include <functional>
#include <algorithm>
#include <string>
#include "Util/etype_info.h"
#include "_jgsw_api.h"

namespace Jigsaw {
	namespace Injection {

		/// <summary>
		/// Provider properties provided
		/// </summary>
		struct JGSW_API knob_provider_properties {
			std::string _namespace = "GLOBAL";
			std::vector<std::string> import_namespaces;
		};

		/// <summary>
		/// Base definition struct common between knob_definitions and slot_definitions
		/// </summary>
		struct JGSW_API inj_definition {
			std::string name;
			const Jigsaw::Util::etype_info* type_info;
			std::function<void* ()> reference_getter;
			std::function<bool()> reference_present;

		};

		/// <summary>
		/// Slots need the ability to be filled. Hence the 'reference_setter' property.
		/// </summary>
		struct JGSW_API slot_definition : public inj_definition {
			std::function<void (void*)> reference_setter;

		};

		/// <summary>
		/// Knobs need to have a handle to the initial creation of the knob, the destruction of the knob,
		/// any dependencies needed to initially construct the knob, and a function for providing slot defintions that need to be filled 
		/// post-construction of the knob.
		/// </summary>
		struct JGSW_API knob_definition : public inj_definition {
			std::function<void()> create_reference;
			std::function<bool(std::vector<slot_definition>*)> get_slots = [](std::vector<Jigsaw::Injection::slot_definition>* slot_defs) { return false; };
			std::vector<slot_definition> constructor_dependencies;
			std::vector<Jigsaw::Util::etype_index> super_classes;

		};

		class JigsawSlots;

		JGSW_API const std::vector<slot_definition>& GetSlots(Jigsaw::Injection::JigsawSlots& slots);
	}
}
#endif