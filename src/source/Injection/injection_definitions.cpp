#include "injection_definitions.h"

#include "Injection/JigsawSlots.h"

namespace Jigsaw {
	namespace Injection {
		const std::vector<slot_definition>& GetSlots(Jigsaw::Injection::JigsawSlots& slots) {
			return slots.slots;
		}
	}
}

