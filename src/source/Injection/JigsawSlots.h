#ifndef _JIGSAW_SLOTS_H_
#define _JIGSAW_SLOTS_H_

#include "Injection/injection_definitions.h"
#include "Util/DTO.h"
#include "_jgsw_api.h"

namespace Jigsaw {
	namespace Injection {

		/// <summary>
		/// JigsawSlots is the base class for any component or Knob that is meant to be filled
		/// with the JigsawContext at time of initialization
		/// </summary>
		class JGSW_API JigsawSlots {
		public:
			friend const std::vector<Jigsaw::Injection::slot_definition>& GetSlots(Jigsaw::Injection::JigsawSlots& slots);

			virtual ~JigsawSlots();

		protected:

			std::vector<slot_definition> slots;

		};
	}
}
#endif