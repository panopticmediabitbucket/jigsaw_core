#include "Injection/JigsawContextConfig.h"
#include "Marshalling/JigsawMarshalling.h"

namespace Jigsaw {
	namespace Injection {
		START_REGISTER_SERIALIZABLE_CLASS(JigsawContextConfig)
		REGISTER_SERIALIZABLE_VECTOR(JigsawContextConfig, std::string, package_names)
		END_REGISTER_SERIALIZABLE_CLASS(JigsawContextConfig)
	}
}