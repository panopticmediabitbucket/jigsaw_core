#ifndef _JIGSAW_CONTEXT_H_
#define _JIGSAW_CONTEXT_H_

#include "Injection/JigsawContextConfig.h"
#include "Injection/JigsawKnobProvider.h"
#include "Injection/JigsawSlots.h"
#include "Injection/injection_definitions.h"
#include "Ref.h"

namespace Jigsaw {
	namespace Injection {

		/// <summary>
		/// Final definition of a knob provider must provide a handle to construct the provider as well as 
		/// any associated properties.
		/// </summary>
		struct JGSW_API knob_provider_definition {
			std::function<JigsawKnobProvider* ()> build_provider;
			std::string name;
			struct knob_provider_properties properties;
		};

		/// <summary>
		/// The global context is used when a jigsaw context object is being constructed to fetch packages and providers
		/// of JigsawKnobs.
		/// </summary>
		class JGSW_API JigsawGlobalContext {
		public:
			/// <summary>
			/// Used statically to submit a globally-scoped provider
			/// </summary>
			/// <param name="provider"></param>
			static void SubmitProvider(const knob_provider_definition& provider);

			/// <summary>
			/// Returns the Knobs associated with a given namespace
			/// </summary>
			/// <param name="_namespace"></param>
			/// <returns></returns>
			static std::vector<knob_provider_definition> GetNamespaceProviders(const std::string& _namespace);

		};

		/// <summary>
		/// JigsawContext finds all of the knob_definitions specified using the parameters provided in the JigsawContextConfig.
		/// 
		/// Dependencies are constructed in order according to their needs at the time of construction. Slots can be filled 
		/// by passing the corresponding slot_definition to the 'FillSlot' method.
		/// </summary>
		class JigsawContext {
		public:
			/// <summary>
			/// The externally-visible function that instantiates a JigsawContext.
			/// </summary>
			/// <param name="config"></param>
			/// <returns></returns>
			static Jigsaw::Ref<JigsawContext> Create(const JigsawContextConfig& config);

			/// <summary>
			/// Returns the first knob of the given type 'T'. Should only be used if there is one knob of the given type
			/// </summary>
			/// <typeparam name="T"></typeparam>
			/// <returns></returns>
			template <typename T>
			Jigsaw::Ref<T> GetKnob() {
				const Jigsaw::Util::etype_info& type_info = Jigsaw::Util::etype_info::Id<T>();
				Jigsaw::Util::etype_index index(type_info);
				Jigsaw::Injection::knob_definition def;
				def.type_info = &type_info;

				int match_index = -1;
				for (int i = 0; i < knobs.size(); i++) {
					Jigsaw::Injection::knob_definition& knob_def = knobs.at(i);
					if (knob_def.type_info == def.type_info || std::find(knob_def.super_classes.begin(), knob_def.super_classes.end(), index) != knob_def.super_classes.end()) {
						match_index = i;
					}
				}
				return *static_cast<Jigsaw::Ref<T>*>(knobs.at(match_index).reference_getter());
			}

			/// <summary>
			/// Searches the JigsawContext for a knob that matches the requirements of the slot_definition, and proceeds to fill the slot.
			/// 
			/// If the slot is not found, false is returned. If multiple matching slots are found, behavior is undefined. In Debug mode, an exception is thrown. 
			/// </summary>
			/// <param name="definition"></param>
			/// <returns></returns>
			bool FillSlot(const Jigsaw::Injection::slot_definition& definition) const;

			/// <summary>
			/// Iterates through the list of slot_definitions, filling each of them along the way with the knobs registered in the
			/// context.
			/// </summary>
			/// <param name="slot_definitions"></param>
			void FillSlots(const std::vector<Jigsaw::Injection::slot_definition>& slot_definitions, const std::string& receiver_name) const;

			/// <summary>
			/// Fills the slots of the passed in JigsawSlots class
			/// </summary>
			/// <param name="slots_class"></param>
			void FillSlots(Jigsaw::Injection::JigsawSlots& slots_class) const;

		protected:
			/// <summary>
			/// This internal function is called during the Create flow, and it's responsible for importing all of the namespaces,
			/// instantiating all of the knobs, and placing them in their appropriate slots. 
			/// </summary>
			/// <param name="self_reference"></param>
			/// <param name="config"></param>
			void Init(Jigsaw::Weak<JigsawContext> self_reference, const JigsawContextConfig& config);

			/// <summary>
			/// The internal constructor just creates a blank context
			/// </summary>
			/// <param name="config"></param>
			JigsawContext();

			std::vector<std::string> namespaces;
			std::vector<Jigsaw::Injection::knob_definition> knobs;
			Jigsaw::Weak<JigsawContext> self;
			
		};

	}
}
#endif