#ifndef _ASYNC_JOB_H_
#define _ASYNC_JOB_H_

#include <windows.h>
#include <WinBase.h>
#include <synchapi.h>
#include <string>
#include <future>
#include "Ref.h"

namespace Jigsaw {
	namespace System {

		/// <summary>
		/// ASYNC_EVENT is a simple wrapper for event handling that is meant to be system-independent,
		/// implementation details are defined internally
		/// </summary>
		class ASYNC_EVENT {
		public:
			ASYNC_EVENT(LPCSTR str);

			/// <summary>
			/// Default constructor is invalid
			/// </summary>
			ASYNC_EVENT() = delete;

			/// <summary>
			/// Copy constructor is invalid
			/// </summary>
			/// <param name="other"></param>
			ASYNC_EVENT(const ASYNC_EVENT& other) = delete;

			/// <summary>
			/// Move onstructor
			/// </summary>
			/// <param name="other"></param>
			ASYNC_EVENT(ASYNC_EVENT&& other) noexcept;

			/// The destructor will tear down any system resources
			~ASYNC_EVENT();

			/// <summary>
			/// Notifies the underlying handle that the event is complete
			/// </summary>
			void Notify();


			/// <summary>
			/// Wait for the underlying event to be triggered
			/// </summary>
			void Await() const;

			/// <summary>
			/// Checks if the event is triggered
			/// </summary>
			/// <returns></returns>
			bool Check() const;

			/// <summary>
			/// Returns the time that the event was triggered
			/// </summary>
			/// <returns></returns>
			const std::chrono::system_clock::time_point& GetActivationTime() const;

		protected:
			std::chrono::system_clock::time_point activation_time;

			const HANDLE handle;

		};

		/// <summary>
		/// Provides a view into an existing ASYNC_EVENT object. 
		/// </summary>
		class EVENT_LISTENER {
		public:
			EVENT_LISTENER(const Jigsaw::Ref<ASYNC_EVENT>& _event);

			/// <summary>
			/// Awaits the completion of the underlying ASYNC_EVENT
			/// </summary>
			void Await() const;

			/// <summary>
			/// Checks if the underlying ASYNC_EVENT has been triggered
			/// </summary>
			/// <returns></returns>
			bool Check() const;

			/// <summary>
			/// Returns the activation time of the underlying event. 
			/// </summary>
			/// <returns></returns>
			const std::chrono::system_clock::time_point& GetActivationTime() const;

		private:
			Jigsaw::Ref<ASYNC_EVENT> _event;

		};

		/// <summary>
		/// Specifies the notification mode of the ASYNC_JOB
		/// </summary>
		enum NOTIFY {
			ASYNC_NOTIFY, // The notification will happen automatically when the ASYNC_JOB is complete
			OWNER_NOTIFY // The owner of the async job is responsible for notifying listeners that the job is complete
		};

		/// <summary>
		/// Simple base class only designed to locally encapsulate the parameters needed for a job. 
		/// </summary>
		/// <typeparam name="T">The return type of the async job</typeparam>
		template<class T>
		class ASYNC_JOB {

		public:
			/// <summary>
			/// On construction, an ASYNC_EVENT is created and the job is launched. By default, 'notify' is set to ASYNC_NOTIFY,
			/// meaning that the ASYNC_JOB will 'Notify' listeners of the ASYNC_EVENT as soon as 'Execute' is complete.
			/// 
			/// If 'notify' is set to OWNER_NOTIFY, then the creator/owner of this job is responsible for notifying listeners
			/// that the job is complete. 
			/// </summary>
			/// <param name="name"></param>
			/// <param name="notify"></param>
			ASYNC_JOB(std::string name, NOTIFY notify = ASYNC_NOTIFY) {
				_event = Jigsaw::MakeRef<ASYNC_EVENT>(name.c_str());
				listener = Jigsaw::MakeRef<EVENT_LISTENER>(_event);
				if (notify == ASYNC_NOTIFY) {
					fut = std::async(std::launch::async, &ASYNC_JOB<T>::ExecuteNotify, this, _event.get());
				}
				else { // OWNER_NOTIFY, the creator of this job takes responsibility for notifying listeners of its completion
					fut = std::async(std::launch::async, &ASYNC_JOB<T>::Execute, this);
				}
			}

			/// <summary>
			/// Checks if the job is complete
			/// </summary>
			/// <returns></returns>
			virtual inline bool Ready() {
				return fut.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
			}

			/// <summary>
			/// Waits for the job to complete and then returns the value.
			/// </summary>
			/// <returns></returns>
			virtual T Await() {
				fut.wait();
				return Get();
			}

			/// <summary>
			/// Gets the value from the completed job.
			/// </summary>
			/// <returns></returns>
			virtual T Get() {
				return fut.get();
			}

			/// <summary>
			/// Returns a listener to the event.
			/// </summary>
			/// <returns></returns>
			Jigsaw::Ref<const EVENT_LISTENER> GetListener() const {
				return listener;
			}

			/// <summary>
			/// Gets the underlying event.
			/// </summary>
			/// <returns></returns>
			Jigsaw::Ref<ASYNC_EVENT>& GetEvent() {
				return _event;
			}

		protected:
			/// <summary>
			/// Abstract method to be implemented by child classes of this ASYNC job
			/// </summary>
			/// <returns></returns>
			virtual T Execute() = 0;

			std::future<T> fut;

		private:
			/// <summary>
			/// Method used in the ASYNC_NOTIFY mode to notify listeners of the job's completion automatically
			/// </summary>
			/// <param name="notify"></param>
			/// <returns></returns>
			T ExecuteNotify(ASYNC_EVENT* notify) {
				T ret = Execute();
				notify->Notify();
				return ret;
			}

			Jigsaw::Ref<ASYNC_EVENT> _event;
			Jigsaw::Ref<EVENT_LISTENER> listener;
		};
	}
}
#endif