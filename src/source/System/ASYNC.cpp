#include "System/ASYNC.h"

namespace Jigsaw {
	namespace System {

			// ASYNC_EVENT implementations 

			ASYNC_EVENT::ASYNC_EVENT(LPCSTR str) : handle(CreateEvent(0, TRUE, FALSE, str)) {}

			ASYNC_EVENT::ASYNC_EVENT(ASYNC_EVENT&& other) noexcept : handle(other.handle), activation_time(other.activation_time) {
				const_cast<HANDLE&>(other.handle) = 0;
			}

			ASYNC_EVENT::~ASYNC_EVENT() {
				CloseHandle(handle);
			}

			void ASYNC_EVENT::Notify() {
				SetEvent(handle);
				activation_time = std::chrono::system_clock::now();
			}

			void ASYNC_EVENT::Await() const {
				WaitForSingleObject(handle, INFINITE);
			}

			bool ASYNC_EVENT::Check() const {
				DWORD d_ = WaitForSingleObject(handle, 0);
				return d_ == WAIT_OBJECT_0;
			}

			const std::chrono::system_clock::time_point& ASYNC_EVENT::GetActivationTime() const {
				return activation_time;
			}

			// EVENT_LISTENER implementations

			EVENT_LISTENER::EVENT_LISTENER(const Jigsaw::Ref<ASYNC_EVENT>& _event) : _event(_event) {}

			void EVENT_LISTENER::Await() const {
				_event->Await();
			}
			
			bool EVENT_LISTENER::Check() const {
				return _event->Check();
			}

			const std::chrono::system_clock::time_point& EVENT_LISTENER::GetActivationTime() const {
				return _event->GetActivationTime();
			}
	}
}