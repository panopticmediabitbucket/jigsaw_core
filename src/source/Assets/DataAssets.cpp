#include "DataAssets.h"

#include "Marshalling/JigsawMarshalling.h"
#include <d3dcompiler.h>
#include "Marshalling/JSONNode.h"

using namespace Jigsaw::Assets;
using namespace Jigsaw::File_IO;
using namespace Jigsaw::Marshalling;

// ShaderResource implementations

ASSET_LOAD_RESULT ShaderResource::Load(const FILE_DATA& file_data, Jigsaw::System::THREAD_SAFE_SYSTEM_RESOURCES sys_resources) {
	HRESULT hresult = D3DReadFileToBlob(file_data.file_name, &shader);

	if (FAILED(hresult)) {
		Jigsaw::Graphics::DX_Context::PrintDebugMessages();
	}

	bytecode_desc.BytecodeLength = shader->GetBufferSize();
	bytecode_desc.pShaderBytecode = shader->GetBufferPointer();

	ASSET_LOAD_RESULT result;
	result.result = COMPLETE;
	return result;
}

ShaderResource::~ShaderResource() { }

JsonAsset::~JsonAsset() {
	if (object) {
		bool delete_res = t_info.GetUnsafeFunctions().Delete(object);
		J_D_ASSERT_LOG_ERROR(delete_res, JsonAsset, "Failed to delete JsonAsset object");
	}
}

void* Jigsaw::Assets::JsonAsset::GetRawData() {
	return object;
}

const Jigsaw::Util::etype_info& Jigsaw::Assets::JsonAsset::GetTypeInfo() {
	return t_info;
}

ASSET_LOAD_RESULT Jigsaw::Assets::JsonAsset::Load(const Jigsaw::File_IO::FILE_DATA& file_data, Jigsaw::System::THREAD_SAFE_SYSTEM_RESOURCES sys_resources) {
	ASSET_LOAD_RESULT result;
	std::istringstream ss(GetStringStreamFromFile(file_data));

	JSONNodeReader json_reader(ss, MarshallingRegistry::GetMarshallingMap(t_info));
	UNMARSHALLING_RESULT u_result = json_reader.BuildNode();

	object = u_result.raw_data;
	result.unresolved_references = u_result.unresolved_references;
	result.result = u_result.result;

	return result;
}

D3D12_VERTEX_BUFFER_VIEW Jigsaw::Assets::Cube::GetVBV()
{
	D3D12_VERTEX_BUFFER_VIEW vbv;
	vbv.BufferLocation = vert_buffer_->GetResource()->GetGPUVirtualAddress();
	vbv.SizeInBytes = vbuff_size;
	vbv.StrideInBytes = sizeof(PositionColorNormal);
	return vbv;
}

D3D12_INDEX_BUFFER_VIEW Jigsaw::Assets::Cube::GetIBV()
{
	D3D12_INDEX_BUFFER_VIEW ibv;
	ibv.BufferLocation = ind_buffer_->GetResource()->GetGPUVirtualAddress();
	ibv.Format = DXGI_FORMAT_R32_UINT;
	ibv.SizeInBytes = ind_array_size;
	return ibv;
}

ASSET_LOAD_RESULT Jigsaw::Assets::Cube::Load(const Jigsaw::File_IO::FILE_DATA& file_data, Jigsaw::System::THREAD_SAFE_SYSTEM_RESOURCES sys_resources)
{
	PositionColorNormal pos_col_array[]{
		{ Vector3(-1, -1, -1), Vector3(1, 0, 0) , Vector3(0, 0, -1) }, { Vector3(1, -1, -1), Vector3(1, 1, 0), Vector3(0, 0, -1) },
		{ Vector3(-1, 1, -1), Vector3(0, 1, 1), Vector3(0, 0, -1) }, { Vector3(1, 1, -1), Vector3(0, 1, 1), Vector3(0, 0, -1) },
	{ Vector3(1, -1, -1), Vector3(1, 1, 0), Vector3(1, 0, 0) }, { Vector3(1, 1, 1), Vector3(1, 1, 0), Vector3(1, 0, 0) },
	{ Vector3(1, 1, -1), Vector3(0, 1, 1), Vector3(1, 0, 0) }, { Vector3(1, -1, 1), Vector3(1, 1, 0), Vector3(1, 0, 0) },
	{ Vector3(-1, -1, 1), Vector3(1, 0, 0), Vector3(0, 0, 1) }, { Vector3(-1, 1, 1), Vector3(1, 0, 0), Vector3(0, 0, 1), },
	{ Vector3(1, 1, 1), Vector3(1, 1, 0), Vector3(0, 0, 1) }, { Vector3(1, -1, 1), Vector3(1, 1, 0), Vector3(0, 0, 1) },
	{ Vector3(-1, -1, -1), Vector3(1, 0, 0), Vector3(0, -1, 0) }, { Vector3(1, -1, 1), Vector3(1, 1, 0), Vector3(0, -1, 0) },
	{ Vector3(1, -1, -1), Vector3(1, 1, 0), Vector3(0, -1, 0) }, { Vector3(-1, -1, 1), Vector3(1, 0, 0), Vector3(0, -1, 0) },
	{ Vector3(-1, 1, -1), Vector3(0, 1, 1), Vector3(-1, 0, 0) }, { Vector3(-1, -1, 1), Vector3(1, 0, 0), Vector3(-1, 0, 0) },
	{ Vector3(-1, -1, -1), Vector3(1, 0, 0), Vector3(-1, 0, 0) }, { Vector3(-1, 1, 1), Vector3(1, 0, 0), Vector3(-1, 0, 0) },
	{ Vector3(-1, 1, 1), Vector3(1, 0, 0), Vector3(0, 1, 0) }, { Vector3(-1, 1, -1), Vector3(0, 1, 1), Vector3(0, 1, 0) },
	{ Vector3(1, 1, -1), Vector3(0, 1, 1), Vector3(0, 1, 0) }, { Vector3(1, 1, 1), Vector3(1, 1, 0), Vector3(0, 1, 0) }
	};

	UINT ind_array[]{
		1, 3, 0, // bottom face
		0, 3, 2,
		4, 5, 6, // back face
		4, 7, 5,
		8, 9, 10, // top face
		8, 10, 11,
		12, 13, 14, // right face
		12, 15, 13,
		16, 17, 18, // front face
		16, 19, 17,
		20, 21, 22, // left face
		20, 22, 23
	};

	sys_resources.cmd_list->LoadBuffer((BYTE*)pos_col_array, sizeof(PositionColorNormal), ARRAYSIZE(pos_col_array), &vert_buffer_);

	sys_resources.cmd_list->LoadBuffer((BYTE*)ind_array, sizeof(UINT), ARRAYSIZE(ind_array), &ind_buffer_);

	ASSET_LOAD_RESULT result;
	result.result = COMPLETE;

	return result;
}