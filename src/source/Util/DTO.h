#ifndef _DTO_H_
#define _DTO_H_

#include "_jgsw_api.h"

namespace Jigsaw {
	namespace Util {

		template <typename T>
		class JGSW_API DTO {
		protected:
			typedef T _THIS_TYPE;
		};
	}
}
#endif