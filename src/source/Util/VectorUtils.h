#ifndef _VECTOR_UTILS_H_
#define _VECTOR_UTILS_H_

#include <vector>
#include <functional>

namespace std {
	template<typename T, typename V>
	inline std::vector<V> map_to(std::vector<T, std::allocator<T>>& initial, std::function<V(T&)>&& func) {
		std::vector<V> vs;
		std::for_each(initial.begin(), initial.end(), [&](T t) { vs.push_back(func(t)); });
		return vs;
	}
}
#endif