struct WORLD_LIGHT {
	int type;
	float intensity;
	float angle;

	float3 position;
	float3 direction;
	float3 color;
};

cbuffer LIGHT_CONSTANT_BUFFER : register(b0) {
	WORLD_LIGHT light_a;
}

struct PIX_INPUT {
	float4 position	: SV_POSITION;
	float4 color	: COLOR0;
	float4 normal	: NORMAL0;
};

float4 main(PIX_INPUT input) : SV_TARGET{
	/**
	if (light_a.type == 1) {

	float p = dot(light_a.direction.xyz, -input.normal);
	p = p < 0 ? 0 : p;
	return (.5f + (p * light_a.intensity)) * input.color;
	}
return float4(0, 0, 0, 1);
*/
	return input.color;
}